import { useEffect, useState, useRef } from "react";

function useSticky() {
  const [isSticky, setSticky] = useState(false);
  const element = useRef(null);

  const handleScroll = () => {
    const yOffset = document.documentElement.scrollTop;
    if (yOffset > 0 && isSticky) return null;
    const stickyFlag = !!yOffset;
    console.log(yOffset);
    return setSticky(stickyFlag);
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", () => handleScroll);
    };
  }, [handleScroll]);

  return [isSticky, element];
}

export default useSticky;
