import React from 'react';
import Head from 'next/head';
import { createGlobalStyle } from 'styled-components';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'video-react/dist/video-react.css';

import { Container, WarningBanner, Footer } from '../components';

function MyApp({ Component, pageProps }) {
  const GlobalStyles = createGlobalStyle`
    *,*::before, *::after{
  margin: 0;
  padding: 0;
  box-sizing: inherit;
}

html{
}

body{
  box-sizing: border-box;
  font-family: 'Montserrat', sans-serif;
  color: #434343;
}
    `;
  return (
    <>
      <Head>
        <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
          rel="stylesheet"
        />
      </Head>
      <GlobalStyles />
      <WarningBanner />
      <Container>
        <Component { ...pageProps } />
        <Footer />
      </Container>
    </>
  );
}

export default MyApp;
