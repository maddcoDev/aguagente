import React from 'react';
import { useRouter } from 'next/router';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';
import BCarousel from 'react-bootstrap/Carousel';
import { useSpring, animated } from 'react-spring';

import UsContainer from '../PageStyles/usStyles';
import { SocialMediaGroup } from '../components';
import WhatWeDoSrc from '../assets/us/quehacemos1.png';
import WhatWeDo2Src from '../assets/us/quehacemos2.png';
import BOneSrc from '../assets/us/B-01.png';
import BCert from '../assets/us/B-empresa.png';
import CurlySrc from '../assets/us/lineas.png';
import IconSrc from '../assets/us/icon.png';
import EcoFriendlySrc from '../assets/us/MA1.png';
import EcoFriendlyDotSrc from '../assets/us/MA2.png';
import guySrc from '../assets/home/guy1.jpg';
import videoGalerySrc from '../assets/us/galeria.png';
import logo from '../assets/aguagente-logo.png';
import misionVector from '../assets/us/misionVector.png';

const Us = () => {
  const router = useRouter();
  const PrevIcon = <span className="display-3 text-dark">&#8249;</span>;
  const NextIcon = <span className="display-3 text-dark">&#8250;</span>;
  const images = [
    {
      id: 1,
      src: guySrc,
    },
    {
      id: 2,
      src: guySrc,
    },
    {
      id: 3,
      src: guySrc,
    },
    {
      id: 4,
      src: guySrc,
    },
    {
      id: 5,
      src: guySrc,
    },
  ];

  const renderImages = images.map((image) => (
    <div className="px-3" key={image.id}>
      <Image src={image.src} className="image" />
    </div>
  ));

  // Todo
  const animation = useSpring({ scroll: 200, from: { scroll: 0 } });

  return (
    <UsContainer fluid className="us" id="us-element">
      <animated.div scrollTop={animation.scroll}>
        <Container fluid className="us__hero mx-0">
          <Row>
            <Col lg={8} className="hero__description d-flex text-white h-100">
              <p className="hero__description__text">
                El RETO DE SER
                {' '}
                <b>UNA EMPRESA B.</b>
              </p>
              <div className="image__container">
                <img src={logo} alt="aguagente logo" />
              </div>
            </Col>
          </Row>
        </Container>
        <Container fluid as="section" className="us__what-we-do">
          <Row className="d-flex justify-content-between title">
            <p className="text-uppercase title__content">
              que
              {' '}
              <b>hacemos</b>
            </p>
            <Image src={WhatWeDo2Src} fluid className="d-none d-lg-block" />
          </Row>
          <Row>
            <Col className="content" lg={5}>
              <p>
                En Aguagente proporcionamos agua purificada ilimitada a las familias al instalar
                nuestro avanzado equipo de purificación de agua en su hogar. Esto por una pequeña
                tarifa mensual que, a menudo, es menor que lo que se gasta en agua embotellada.
              </p>
              <p>La tarifa mensual incluye un cambio de filtros anual y llamadas de servicio.</p>
              <p>
                Es una solución cómoda y sencilla, usando la mas avanzada tecnología de purificación
                de agua y estado de la técnica de TI para dar servicio al cliente excepcional.
              </p>
              <p>
                Con nuestra App para el cliente y nuestro exclusivo programa de referencias,
                ofrecemos la única oportunidad en México d recibir verdaderamente agua purificada
                ilimitada completamente gratis.
              </p>
              <p>
                Una encuesta independiente a nuestros clientes mostró un 95% de satisfacción del
                cliente, siendo las 3 razones mas importantes para elegir nuestro servicio:
                conveniencia (43%), calidad del agua (23%) y precio (12%)
              </p>
            </Col>
            <Col lg={7} className="image__column ">
              <Image src={WhatWeDoSrc} fluid style={{ overflow: 'hidden' }} />
            </Col>
          </Row>
        </Container>
        <Container as="section" fluid className="us__mission mt-5 mt-md-0">
          <Row className="us__mission__container mt-4">
            <Col lg={2} />
            <Col md={12} lg={10} className="data__container">
              <Row className="d-flex flex-column flex-lg-row text-justify">
                <Col lg={3} className="data__container__image">
                  <Image src={misionVector} />
                </Col>
                <Col className="data__container__info ">
                  <Container>
                    <p className="title">Misión</p>
                    <p>
                      Ofrecer la mejor agua purificada y
                      {' '}
                      <b>experiencia de servicio</b>
                      {' '}
                      a nuestros
                      clientes.
                    </p>
                  </Container>
                </Col>
                <Col className="data__container__info">
                  <Container>
                    <p className="title">Visión</p>
                    <p>
                      Eliminar la necesidad de producir y transportar agua embotellada
                      {' '}
                      <b>purificándola en el punto de entrega.</b>
                    </p>
                  </Container>
                </Col>
                <Col className="data__container__info">
                  <Container>
                    <p className="title">Valores</p>
                    <p>
                      Estamos comprometidos a brindar un servicio incomparable tratando a todos por
                      igual, sin causar daño a
                      {' '}
                      <b>nuestro medio ambiente,</b>
                      {' '}
                      mientras hacemos algo
                      bueno para aquellos que lo necesitan.
                    </p>
                  </Container>
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
        <Container fluid as="section" className="us__b-corp px-o">
          <Row className="us__b-corp__container">
            <Col className="image__column" lg={6}>
              <div />
              <Image fluid src={BOneSrc} />
              <Container>
                <p>
                  Las Empresas B son empresas que redefinen el el sentido del exito, poniendo en un
                  mismo nivel el impacto economico, social y ambiental, genrando un triple impacto
                  positivo. Son empresas que utilizn el poder del mercado para dar soluciones
                  concretas a problemas sociales y ambientales.
                </p>
              </Container>
            </Col>
            <Col className="content" lg={6}>
              <Container className="us__b-corp__description">
                <p>
                  Aguagente es una empresa social que ha sido auditada y tiene la cerficacion de
                  Empresa B.
                </p>
                <p className="content__title">
                  Nos convertimos en la 21a empresa en Mexico con lograr la prestigiosa
                  certificacion internacional.
                </p>
                <p>
                  Una empresa social intenta maximizar el impacto social y el bienestar ambiental,
                  asi como obtener ganancias para los accionistas.
                </p>
              </Container>
              <div className="image__container">
                <Image fluid src={BCert} />
                <Image fluid src={CurlySrc} />
              </div>
              <p>¿Quieres conocer más sbore el sistema B?</p>
              <Container className="d-flex justify-content-center">
                <a href="#" className="custom__button">
                  Haz click aquí
                  <Image src={IconSrc} />
                </a>
              </Container>
            </Col>
          </Row>
        </Container>
        <Container fluid as="section" className="us__eco-friendly">
          <Row>
            <Image fluid src={CurlySrc} />
          </Row>
          <Row className="content">
            <Col className="content__info" lg={6}>
              <Container>
                <p className="title">
                  <b>RESPONSABLES</b>
                  {' '}
                  CON EL
                  <b> MEDIO AMBIENTE</b>
                </p>
                <p>
                  En los últimos años, sofisticadas campañas de publicidad han alejado a los
                  consumidores del agua del grifo representando a las marcas de agua embotelladas
                  como puras, sanas y de moda. Muchas han ensalzado las virtudes del entorno de
                  donde proviene el agua.
                </p>
                <p>
                  Nos hemos creído la idea de que el agua embotellada es superior al agua del grifo
                  que ahora es la industria de bebidas de más rápido crecimiento en el mundo, con un
                  consumo mundial de más de 87 bn de litros. Más del 11% se consume aquí mismo en
                  Mexico
                </p>
                <p>
                  La fabricación de una tonelada de pET produce alrededor de tres toneladas de CO2.
                  A nivel mundial, la industria utiliza 1.5 millones de toneladas de plástico al
                  año, emitiendo 4.5m de toneladas de CO2. Como el agua es pesada, su transporte
                  también genera altas emisiones.
                </p>
                <p>
                  El agua de la llave, por otro lado, tiene una huella de carbono mucho menor.
                  Cuando a esto se le suma el hecho de que tres cuartas partes de las botellas de
                  plástico terminan en vertederos de basura doméstica, comprar agua embotellada es
                  un desastre ambiental.
                </p>
                <p>
                  Beber agua de la llave purificada al momento en tu propia casa hará una
                  diferencia.
                </p>
              </Container>
            </Col>
            <Col className="content__image" lg={6}>
              <Image src={EcoFriendlyDotSrc} className="dot-image d-none d-lg-block" />
              <Image src={EcoFriendlySrc} fluid className="d-block d-lg-none" />
            </Col>
          </Row>
        </Container>
        <Container as="section" fluid className="us__mexico__better">
          <Container className="us__mexico__better__header">
            <div className="title__group">
              <p className="title">
                Hagamos México
                <br />
                <b>mejor, juntos.</b>
              </p>
              <Image src={logo} fluid />
            </div>
            <div className="header__iamge" />
          </Container>
          <Container className="us__mexico__better__body">
            <p>
              En Aguagente queremos hacer una diferencia en México. Más de la mitad de nuestros
              ingresos van directamente a la pobalción; siempre tratamos de utilizar proveedores
              locales cuando podamos, sin comprometer precio, calidad o servicio al cliente.
            </p>
            <p className="accent">
              Además nos hemos comprometido a donar el 0.7% de nuestros ingresos a la
              Responsabilidad Social.
            </p>
            <p>
              0.7% se refiere a las repetidas promesas incumplidas de los gobiernos de los paises
              mas ricos del mundo, de coomprometer el 0.7% del producto nacional bruto (PNB) a la
              Ayuda Oficial al Desarrollo. Prometdio en primer lugar hace 50 años a las Naciones
              Unidas en 1970 en una Resolución de la Asamblea General. El objetivo del 0.7% ha sido
              afirmado en muchos acuerdos internacionales en los últimos años, incluídos entre
              otrosÑ la Conferencia Internacional de Marzo 2002 sobre la Financiación para el
              Desarollo en Monterey, México y la Cumbre Mundial sobre el Desarollo Sostenible,
              celebrada en Johannesburgo del 26 de agosto al 4 de septiembre de 2002 organizada por
              la ONU.
            </p>
            <p>
              Según el sitio web del Proyecto del Milenio de las Naciones Unidas, solo 5 de los 22
              paises han alcanzado o superado el objetivo del 0.7% que se acordó hace 50 años.
            </p>
            <p>
              Si en Aguagente podemos hacerlo, tal vez otros nos sigan.
              {' '}
              <b>Vamos a hacer un Mexico mejor, juntos...</b>
            </p>
          </Container>
        </Container>
        <Container as="section" fluid className="us__techo__header mt-3">
          <Row className="image__container">
            <Col lg={6}>image</Col>
            <Col lg={6}>image2</Col>
          </Row>
          <Container className="content">
            <p className="content__text">
              ¿Quieres saber más sobre
              {' '}
              <b>TECHO</b>
              ?
            </p>
            <a className="custom__button" href="#">
              Haz click aquí
            </a>
          </Container>
        </Container>
        <Container fluid as="section" className="us__techo__content">
          <Row className="header">
            <Container>
              <p>
                QUIENES
                {' '}
                <b>SOMOS</b>
              </p>
            </Container>
          </Row>
          <Row className="carousel">
            <BCarousel prevIcon={PrevIcon} nextIcon={NextIcon} className="carousel__body w-100">
              <BCarousel.Item className="py-5">
                <Container className="text-center d-flex flex-column flex-lg-row justify-content-around">
                  {renderImages}
                </Container>
              </BCarousel.Item>
              <BCarousel.Item className="py-5">
                <Container className="text-center d-flex flex-column flex-lg-row justify-content-around">
                  {renderImages}
                </Container>
              </BCarousel.Item>
            </BCarousel>
          </Row>
          <Row className="video__section mt-5">
            <Col className="video__section__grid" xl={9}>
              <Row className="grid__row">
                <Col xl={2} className="d-flex justify-content-center">
                  <Image fluid src={videoGalerySrc} className="d-block d-xl-none" />
                </Col>
                <Col xl={10}>
                  <Row>
                    <Col sm={6} md={6} lg={4}>
                      <div className="video__placeholder" />
                    </Col>
                    <Col sm={6} md={6} lg={4}>
                      <div className="video__placeholder" />
                    </Col>
                    <Col sm={6} md={6} lg={4}>
                      <div className="video__placeholder" />
                    </Col>
                    <Col sm={6} md={6} lg={4}>
                      <div className="video__placeholder" />
                    </Col>
                    <Col sm={6} md={6} lg={4}>
                      <div className="video__placeholder" />
                    </Col>
                    <Col sm={6} md={6} lg={4}>
                      <div className="video__placeholder" />
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
            <Col className="video__section__grid background-image" xl={3} />
          </Row>
          <Row className="more-videos">
            <Container fluid="md" className="more-videos__content">
              <p>
                <b>Ver mas</b>
                {' '}
                videos en
              </p>
              <a href="#" className="custom__button">
                <span>&#11208;</span>
                {' '}
                Aguagente
                <em className="ml-2"> TV</em>
              </a>
            </Container>
            <Container className="text-center social-media__group">
              <p>quieres contarle a alguien sobre Aguagente?</p>
              <p>puedes compartir esta información</p>
              <p>
                <b>haciendo clic aquí</b>
              </p>
              <SocialMediaGroup />
            </Container>
          </Row>
        </Container>
      </animated.div>
    </UsContainer>
  );
};

export default Us;
