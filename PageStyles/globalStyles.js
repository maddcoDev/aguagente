import { css } from "styled-components";

const primaryColor = "rgb(20, 68, 120)";
const bigBreakpoint = "1470px";
const midBreakpoint = "1040px";
const smallBreakpoint = "920px";
const phoneBreakpoint = "505px";

const getButtonStyles = ({ buttonColor, shadowColor }) => css`
  background-color: ${buttonColor};
  border-radius: 0.5rem;
  transition: all 0.4s ease;
  color: white;
  border: none;
  outline: none;
  display: flex;
  align-items: center;
  box-shadow: 6px 6px 5px 0px ${shadowColor};
  text-decoration: none;
  &:hover {
    background-color: ${shadowColor};
    cursor: pointer;
    transform: translateY(-3px);
  }
`;

export {
  primaryColor as default,
  midBreakpoint,
  smallBreakpoint,
  phoneBreakpoint,
  bigBreakpoint,
  getButtonStyles,
};
