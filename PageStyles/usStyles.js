// @format
import Container from 'react-bootstrap/Container';
import styled from 'styled-components';
import { smallBreakpoint, phoneBreakpoint, getButtonStyles } from './globalStyles';

import BannerSrc from '../assets/us/banner.png';
import EcoFriendlySrc from '../assets/us/MA1.png';
import videoGalerySrc from '../assets/us/galeria.png';
import betterMexico1 from '../assets/us/mexicomejor1.png';

const UsContainer = styled(Container)`
  padding: 0;
  .us {
    &__hero {
      height: 90vh;
      background-image: url(${BannerSrc});
      background-position: center;
      background-size: 100% 100%;
      background-repeat: no-repeat;
      @media (max-width: ${smallBreakpoint}) {
        height: auto;
      }
      & .hero__description {
        padding-top: 7%;
        padding-left: 150px;
        display: flex;
        flex-direction: row;
        & .image__container {
          margin-top: 80px;
          @media (max-width: ${smallBreakpoint}) {
            margin-top: 10px;
          }
        }
        & img {
          height: 200px;
          width: 200px;
          margin-right: 30px;
          @media(max-width: ${phoneBreakpoint}) {
            height: 150px;
            width: 150px;
          }
        }
        @media (max-width: 1100px) {
          padding-left: 60px;
        }
        @media (max-width: ${smallBreakpoint}) {
          flex-direction: column;
          align-items: center;
        }
        @media (max-width: ${phoneBreakpoint}) {
          padding-left: 10px;
        }
        &__text {
          line-height: 1;
          font-size: 90px;
          margin-right: 30px;
          @media (max-width: ${smallBreakpoint}) {
            font-size: 70px;
            margin-top: 45px;
          }
          @media (max-width: ${phoneBreakpoint}) {
            font-size: 30px;
            margin-right: 0;
            text-align: center;
          }
        }
      }
    }

    &__what-we-do {
      margin-top: 35px;
      padding-left: 150px;
      @media (max-width: 992px) {
        padding-left: 15px;
      }
      & .title {
        position: relative;
        & img {
          position: absolute;
          left: 80%;
          top: -30px;
          @media (max-width: ${phoneBreakpoint}) {
            display: none;
          }
        }
        &__content {
          font-size: 50px;
          border-bottom: 5px solid limegreen;
          margin-left: 25px;
          @media (max-width: ${phoneBreakpoint}) {
            font-size: 30px;
            margin: 0 auto;
          }
        }
      }
      & .content {
        font-size: 20px;
        @media (max-width: ${phoneBreakpoint}) {
          text-align: center;
          margin-top: 10px;
          font-size: 16px;
        }
      }
      & .image__column {
        position: relative;
        & img {
          &:first-of-type {
            /* height: 100%; */
            width: 100%;
            position: absolute;
            top: 0;
            @media (max-width: 992px) {
              position: relative;
            }
          }
        }
      }
    }

    &__mission {
      &__container {
        & .data__container {
          position: relative;
          background-color: #4da8f1;
          border-radius: 7px;
          height: 450px;
          padding: 0 50px;
          padding-left: 0;
          @media (max-width: 991px) {
            padding: 50px 20px;
            height: auto;
          }
          &__image {
            & img {
              position: absolute;
              left: -180px;
              @media (max-width: 991px) {
                left: 0;
                display: none;
              }
            }
          }
          &__info {
            color: white;
            padding-top: 20px;
            & .title {
              font-size: 35px;
              font-weight: 500;
            }
            @media (max-width: 991px) {
              font-size: 14px;
              text-align: center;
            }
          }
        }
      }
    }
    &__b-corp {
      margin-top: 50px;
      &__description {
      }
      &__container {
        & .image__column {
          display: flex;
          flex-direction: column;
          justify-content: space-between;

          & p {
            padding: 0 40px;
            margin-bottom: 0;
            font-size: 20px;
            text-align: center;
            @media (max-width: ${phoneBreakpoint}) {
              font-size: 16px;
            }
          }
        }
        & .content {
          text-align: center;
          font-size: 20px;
          padding-right: 200px;
          &__title {
            font-weight: bold;
            font-size: 35px;
          }
          @media (max-width: 1200px ) {
            padding-right: 50px;
          }
          @media (max-width: ${phoneBreakpoint}) {
            margin-top: 10px;
            font-size: 16px;
            padding-right: 0;
            &__title {
              font-size: 20px;
            }
          }
          & .image__container {
            position: relative;
            & img {
              position: relative;
              z-index: 2;
              &:last-of-type {
                position: absolute;
                z-index: 1;
                top: 40%;
                left: 80%;
                @media (max-width: 1025px) {
                  display: none;
                }
              }
              @media (max-width: 992px) {
                &:last-of-type {
                  position: relative;
                  display: none;
                }
              }
            }
          }
          & .custom__button {
            ${getButtonStyles({
    buttonColor: '#AFFFFF',
    shadowColor: '#5ED3FF',
  })}
            color: black;
            padding: 10px 15px;
            font-size: 30px;
            font-weight: bold;
            text-decoration: none;
            @media (max-width: ${phoneBreakpoint}) {
              font-size: 23px;
            }
          }
        }
      }
    }
    &__eco-friendly {
      margin-top: 20px;
      & .content {
        margin-top: 20px;
        &__info {
          text-align: center;
          font-size: 20px;
          & p {
            padding: 5px 80px;
            text-align: justify;
            &:first-of-type {
              padding: 0;
            }
            @media (max-width: 1536px) {
              padding: 5px 100px;
            }
            @media (max-width: 1300px) {
              padding: 5px 0px;
            }
            @media (max-width: ${phoneBreakpoint}) {
              font-size: 16px;
            }
          }
          & .title {
            font-size: 60px;
            border-bottom: 4px solid #74ba40;
            @media (max-width: 560px) {
              font-size: 30px;
              margin: 0;
            }
          }
        }
        &__image {
          position: relative;
          background-image: url(${EcoFriendlySrc});
          background-size: cover;
          min-height: 100vh;
          display: flex;
          justify-content: center;
          @media (max-width: 992px) {
            min-height: auto;
          }
          & .dot-image {
            position: absolute;
            top: -130px;
            left: 25%;
            height: 250px;
            width: 280px;
          }
          @media (max-width: 991px) {
            background: none;
            height: auto;
          }
        }
      }
    }
    &__mexico__better {
      margin-top: 20px;
      &__header {
        text-align: center;
        
        & .title {
          font-size: 30px;
          color: #0092dd;
        }
        & img {
          height: 150px;
          width: 150px;
        }
        @media(min-width: 767px ) {
          display: flex;
          background-image: url(${betterMexico1});
          background-position: right;
          background-size: contain;
          background-repeat: no-repeat;
          & .title {
            font-size: 37px;
          }
        }
        @media(min-width: 991px) {
          padding-bottom: 100px;
          & .title {
            font-size: 50px;
          }
        }
        @media(min-width: 1200px) {
          & .title {
            font-size: 60px;
          }
        }
      }
      &__body {
        margin-top: 20px;
        text-align: justify;
        & .accent {
          font-weight: bold;
          font-size: 18px;
        }
      }
    }
    &__techo__header {
      & .content {
        display: flex;
        flex-direction: column;
        align-items: center;
        & p {
          font-size: 50px;
          text-align: center;
          @media (max-width: 992px) {
            font-size: 35px;
          }
          @media (max-width: ${phoneBreakpoint}) {
            font-size: 25px;
          }
        }
        & .custom__button {
          ${getButtonStyles({
    buttonColor: '#AFFFFF',
    shadowColor: '#5ED3FF',
  })}
          text-decoration: none;
          color: black;
          font-size: 20px;
          font-weight: bold;
          padding: 10px 20px;
          @media (max-width: ${phoneBreakpoint}) {
            font-size: 1rem;
          }
        }
      }
    }
    &__techo__content {
      margin-top: 20px;
      & .header {
        font-size: 50px;
        @media (max-width: ${phoneBreakpoint}) {
          font-size: 30px;
        }
        & p {
          position: relative;
          &:after {
            content: '';
            width: 400px;
            height: 5px;
            background-color: #74ba40;
            position: absolute;
            top: 100%;
            left: -55px;
            @media (max-width: ${phoneBreakpoint}) {
              width: 250px;
            }
          }
        }
      }
      .carousel__body {
      }
      .image {
        width: 200px;
        height: 200px;
        transition: all 0.2s ease;
        &:hover {
          border: 5px solid #b3feff;
          border-radius: 6px;
          transform: scale(1.5);
          transform-origin: center;
        }
        @media (max-width: 991px) {
          margin-top: 10px;
        }
      }
      & .video__section {
        &__grid {
          &.background-image {
            min-height: 100%;
            background-image: url(${videoGalerySrc});
            background-size: cover;
            background-repeat: no-repeat;
            background-position: 5px;
          }
          & .grid__row {
            & .video__placeholder {
              height: 200px;
              width: 100%;
              color: white;
              background-color: lightgray;
              content: 'video placeholder';
              margin-right: 10px;
              margin-top: 10px;
            }
          }
        }
      }
      & .more-videos {
        font-size: 20px;
        &__content {
          margin-top: 20px;
          display: flex;
          flex-direction: row;
          justify-content: center;
          align-items: center;
          & p {
            margin-bottom: 0;
            font-size: 30px;
          }

          & .custom__button {
            ${getButtonStyles({
    buttonColor: '#FE0000',
    shadowColor: '#CA4C4C',
  })}
            text-decoration: none;
            font-size: 30px;
            font-weight: bold;
            padding: 10px 20px;
            margin-left: 20px;
          }
          @media (max-width: 589px) {
            flex-direction: column;
            & p {
              margin-bottom: 10px;
            }
            & .custom__button {
              margin-left: 0;
              font-size: 25px;
            }
          }
        }
        & .social-media__group {
          display: flex;
          flex-direction: column;
          justify-content: center;
          line-height: 0.7;
          margin-top: 50px;
          @media (max-width: ${phoneBreakpoint}) {
            line-height: 1;
          }
          &__icons {
            display: flex;
            justify-content: center;
            margin-top: 20px;
            & .icon {
              width: 50px;
              height: 50px;
              background-color: lightgrey;
              margin-right: 15px;
            }
          }
        }
      }
    }
  }
`;

export default UsContainer;
