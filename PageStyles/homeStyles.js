import styled, { css } from 'styled-components';
import PromoImage from '../assets/home/promo2.png';
import VideoSectionImage from '../assets/home/video1.png';
import DotsBackground from '../assets/home/puntos.png';
import CuerpoSanoImg from '../assets/home/cuerpo-sano.png';
import CovidImg from '../assets/home/acciones-covid-fondo.png';
import linesImg from '../assets/home/lineas.png';
import {
  phoneBreakpoint, smallBreakpoint, midBreakpoint, getButtonStyles,
} from './globalStyles';

const borderShadow = css`
  border-radius: 0.5rem;
  box-shadow: 0px 6px 15px -10px rgba(0, 0, 0, 0.75);
`;

const HomeContent = styled.div`
  @media (max-width: ${phoneBreakpoint}) {
    .row {
      margin: 0 !important;
    }
  }
  & .registration-add__section {
    background-image: url(${PromoImage});
    background-size: cover;
    background-repeat: no-repeat;
    padding-left: 5%;
  }
  & .registration-add {
    @media (max-width: ${smallBreakpoint}) {
      h1 {
        font-size: 40px !important;
        & span {
          font-size: 40px !important;
        }
      }
      &__info {
        margin-top: 0px !important;
        & p {
          padding-top: 5px !important;
        }
        &__promotion-price {
          & span {
            font-size: 100px !important;
            &:last-of-type {
              font-size: 140px !important;
            }
          }
        }
      }
    }

    @media (max-width: ${phoneBreakpoint}) {
      & h1 {
        text-align: center;
        font-size: 20px !important;
        & span {
          font-size: 25px !important;
        }
      }
      &__info {
        & p {
          font-size: 22px !important;
          & span {
            font-size: 35px !important;
            &:after {
              width: 60px !important;
            }
            &:before {
              width: 60px !important;
            }
          }
        }
        &__promotion-price {
          & span {
            font-size: 70px !important;
            &:last-of-type {
              font-size: 100px !important;
            }
          }
        }
      }
      & .custom-button__green {
        padding: 10px !important;
        font-size: 20px !important;
        margin: 0 auto !important;
      }
    }

    min-height: 100vh;
    width: 100%;

    & h1 {
      padding-top: 50px;
      font-size: 60px;
      font-weight: 400;
      & span {
        font-size: 65px;
        font-weight: 700;
      }
    }
    &__info {
      color: white;
      font-size: 50px;
      margin-top: 40px;
      & p {
        margin-left: 5%;
        &:first-of-type {
          & span {
            position: relative;
            font-size: 55px;
            font-weight: 500;
            &:after {
              position: absolute;
              top: 50%;
              left: 0;
              content: '';
              height: 4px;
              width: 100px;
              background-color: red;
              transform: rotate(30deg);
            }
            &:before {
              position: absolute;
              top: 50%;
              left: 0;
              content: '';
              height: 4px;
              width: 100px;
              background-color: red;
              transform: rotate(-30deg);
            }
          }
        }
      }
      &__promotion-price {
        display: flex;
        align-items: center;
        line-height: 0.9;
        & span {
          font-size: 150px;
          font-weight: 700;
          &:last-of-type {
            font-size: 250px;
          }
        }
      }
    }
  }
  & .know-us {
    width: 100%;
    height: 60vh;
    background-image: url(${VideoSectionImage}), url(${DotsBackground});
    background-repeat: no-repeat, no-repeat;
    background-position: left, right bottom;
    background-size: 700px, auto;
    & .main__container {
    }
    &__container {
      width: 100%;
      height: 100%;
      display: flex;
      @media (max-width: ${phoneBreakpoint}) {
        padding-right: 0 !important;
      }
      & .video-player {
        width: 70%;
        display: flex;
        justify-content: center;
        align-items: center;
        &__media {
          display: flex;
          justify-content: center;
          align-items: center;
          font-size: 20px;
          color: white;
          width: 80%;
          height: 70%;
          background-color: lightgrey;
        }
        @media (max-width: ${smallBreakpoint}) {
          background: none;
          &__media {
            width: 100%;
            height: 50%;
          }
        }
        @media (max-width: ${phoneBreakpoint}) {
          margin: 0 auto !important;
          &__media {
            height: 170px;
          }
        }
      }
      & .action__content {
        width: 30%;
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: flex-start;
        @media (max-width: ${phoneBreakpoint}) {
          &,
          .custom-button__red {
            font-size: 20px !important;
            z-index: 1;
          }
        }
        & p {
          font-size: 22px;
          & span {
            font-weight: 700;
          }
        }
      }
    }
  }
  & .content-description {
    background-image: url(${CuerpoSanoImg});
    background-repeat: no-repeat;
    background-position: left;
    background-size: auto;
    @media (max-width: 1200px) {
      background: none;
    }

    &__container {
      display: flex;
      & .image__banner {
        flex: 2;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        @media (min-width: 1200px) {
          justify-content: flex-end;
        }
        & img {
          max-height: 400px;
        }
        @media (max-width: ${smallBreakpoint}) {
          & p {
            text-align: center;
          }
          & img {
            max-height: 400px !important;
          }
        }
        @media (max-width: ${phoneBreakpoint}) {
          & p {
            font-size: 23px !important;
          }
        }
        & img {
          width: 90%;
          height: 20rem;
          flex: 1;
        }
        &__action {
          margin-top: 20px;
          display: flex;
          flex-direction: column;
          align-items: center;
          @media (max-width: ${phoneBreakpoint}) {
            & .custom-button__blue {
              font-size: 24px !important;
            }
          }
          & p {
            font-size: 30px;
            font-weight: 500;
          }
        }
      }
      & article {
        flex: 1;
        & h2 {
          font-size: 45px;
        }
        & p {
          font-weight: 500;
          & .p__title{
            text-align: right;
            @media (max-width: ${phoneBreakpoint}) {
              text-align: center;
            } 
          }
          & p {
            text-align: justify !important;
          }
        }
      }
    }
  }

  & .we-work {
    & h2 {
      font-size: 55px;
    }
  }

  & .testimonials {
    &__title {
      font-size: 45px;
    }
  }

  @media (max-width: ${phoneBreakpoint}) {
    .we-work {
      & h2 {
        font-size: 32px !important;
      }
    }
    .carousel__content {
      & h2 {
        font-size: 45px;
      }
    }
    .covid__section {
      & p {
        font-size: 20px;
      }
      & .text-success {
        & p {
          font-size: 13px !important;
        }
      }
    }
    .wellness {
      & [class*='custom-button'] {
        padding: 10px !important;
        margin-top: 20px !important;
        margin-bottom: 20px !important;
        & b {
          font-size: 20px !important;
        }
      }
    }

    .testimonials {
      &__title {
      }
    }

    .contact__form {
      & h2 {
        font-size: 45px !important;
      }
      & p {
        font-size: 20px !important;
        margin-top: 20px !important;
      }
      @media (max-width: ${phoneBreakpoint}) {
        & .form__input {
          padding-right: 0 !important;
          padding-left: 0 !important;
        }
        & button {
          font-size: 20px !important;
          padding: 15px !important;
        }
      }
    }
  }

  .covid__section {
    background: url(${CovidImg}) no-repeat center center;
    background-size: cover;
      & .first__col{
        padding-left: 160px;
        text-align: justify !important;
        @media(max-width: 1500px) {
          padding-left: 100px;
        }
        @media(max-width: 1200px) {
          padding-left: 0;
        }
        & .covid__title {
          font-size: 40px;
        }
      }
      & .second__col {
        display: flex;
        flex-direction: column;
        justify-content: center;
      }
    @media (max-width: ${midBreakpoint}) {
      & img {
        height: 40% !important;
        width: 100% !important;
      }
    }
    @media (max-width: ${smallBreakpoint}) {
      text-align: center !important;
      background: rgb(0, 119, 232);
      background: linear-gradient(0deg, rgba(0, 119, 232, 1) 0%, rgba(0, 187, 255, 1) 100%);
      & img {
        height: 60% !important;
        width: 100% !important;
      }
    }
    @media (max-width: ${phoneBreakpoint}) {
      & img {
        height: 40% !important;
        width: 100%;
      }
    }
  }

  .carousel__body {
    background-image: url(${linesImg});
    background-repeat: no-repeat;
    background-position: left;
    background-size: 20%;
    background-position-x: -80px;
    background-position-y: 70%;
    @media (max-width: ${phoneBreakpoint}) {
      background: none;
    }
  }

  .commentImage {
    width: 250px;
    height: 250px;
    border: 5px solid #aeffff;
    ${borderShadow}
    @media(max-width: ${phoneBreakpoint}) {
      height: 200px;
      width: 200px;
    }
  }

  .contact__form {
    background-image: url(${DotsBackground});
    background-repeat: no-repeat;
    background-position: left;
    background-position-x: -9%;
  }

  .carousel-indicators li {
    background-color: darkgray;
  }

  .user-icon {
    width: 80px;
    height: 80px;
    border-radius: 50%;
  }

  .want-water {
    position: relative;
    &:after {
      content: '';
      position: absolute;
      height: 4px;
      width: 50%;
      background-color: #99c742;
      top: 95%;
      left: 48%;
    }
  }

  .custom-button {
    &__green {
      ${getButtonStyles({
    buttonColor: '#97C740',
    shadowColor: 'rgba(99, 173, 7, 1)',
  })}
    }
    &__red {
      ${getButtonStyles({
    buttonColor: '#FE0000',
    shadowColor: '#CA4C4C',
  })}
    }
    &__blue {
      ${getButtonStyles({
    buttonColor: '#AFFFFF',
    shadowColor: '#5ED3FF',
  })}
    }
  }
`;

export default HomeContent;
