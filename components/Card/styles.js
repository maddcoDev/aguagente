import styled from "styled-components";
import primaryColor, { smallBreakpoint } from "../../PageStyles/globalStyles";

const StyledDiv = ({ imgSrc, ...rest }) => <div {...rest} />;

const overlayGradient = "rgba(0, 0, 0, 0.47)";

const CardContainer = styled(StyledDiv)`
  height: 100%;
  width: 30rem;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  border: 0.5rem solid ${primaryColor};
  border-radius: 0.7rem;
  box-shadow: 0px 10px 16px -8px rgba(0, 0, 0, 0.72);
  @media (max-width: ${smallBreakpoint}) {
    background: linear-gradient(0deg, ${overlayGradient}, ${overlayGradient}),
      url(${(props) => props.imgSrc});

    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    position: relative;
    ${"" /* &:before {
      position: absolute;
      background-color: black;
      content: "";
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      opacity: 0.4;
    } */}
  }
  & .card {
    &__image {
      width: 100%;
      flex: 2;
      & img {
        height: 22rem;
        width: 100%;
      }
    }
    &__description {
      flex: 2;
      width: 100%;
      padding: 1rem;
      & h3 {
        text-align: center;
        font-weight: 600;
        font-size: 2rem;
      }
      & p {
        font-size: 1.3rem;
      }
    }
    &__footer {
      flex: 1;
      width: 100%;
      display: flex;
      justify-content: center;
      align-items: center;
    }
  }
`;

export default CardContainer;
