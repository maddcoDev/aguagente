import CardContainer from "./styles";
import { Button } from "../index";

const Card = (props) => {
  const { imgSrc, description } = props;
  return (
    <CardContainer className="card" imgSrc={imgSrc}>
      <div className="card__image">
        <img src={imgSrc} />
      </div>
      <div className="card__description">
        <h3>Test title</h3>
        <p>{description}</p>
      </div>
      <div className="card__footer">
        <Button>Leer mas</Button>
      </div>
    </CardContainer>
  );
};

export default Card;
