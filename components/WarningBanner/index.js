import Container from "react-bootstrap/Container";
import styles from "./index.module.css";
import WarningContainer from "./styles";

const WarningBanner = ({ element }) => {
  return (
    <WarningContainer
      fluid
      ref={element}
      className="warning-banner py-3"
      style={{ backgroundColor: "#ffa358", textAlign: "center" }}
    >
      <p className="h5">
        Nuestro compromiso y solidaridad con las familias mexicanas es mas
        fuerte que nunca. Frente a la{" "}
        <b>contingencia del coronavirus (Covid-19),</b> estamos tomando
        distintas medidas para{" "}
        <b>
          garantizar en todo momento la seguridad e integridad de los clientes y
          colaboradores.
        </b>
      </p>
    </WarningContainer>
  );
};

export default WarningBanner;
