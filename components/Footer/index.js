import React from 'react';
import Container from 'react-bootstrap/Container';

import FooterContainer from './styles';

const Footer = () => (
  <FooterContainer as="section" fluid className="us__footer px-0">
    <Container>
      <p>
        conoce nuestro programa de referidos Aguagente.
        {' '}
        <a className="text-white" href="#">
          <b>Recomienda y Gana!</b>
          <b> Aquí</b>
        </a>
      </p>
      <p>
        Aguagente S.A.P.I de C.V Méico 2020. Todos los derechos reservados, prohivida su
        reproducción total o parcial sin la previa autorización de su titular.
        {' '}
        <a className="text-white" href="#">
          <b>Aviso de privacidad</b>
        </a>
      </p>
      <a className="text-white" href="#">
        <b>Contrato de suministro de agua y comodato de equipo</b>
      </a>
    </Container>
  </FooterContainer>
);

export default Footer;
