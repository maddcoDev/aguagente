import styled from 'styled-components';
import Container from 'react-bootstrap/Container';

const SocialMediaContainer = styled(Container)`
 .social-media {
    &__icon {
      width: 50px;
      height: 50px;
      border-radius: 5px;
      transition: all .3s ease;
      &:hover {
        transform: translateY(-3px) rotate(10deg);
      }
      & img {
        height: 100%;
        width: 100%;
      }
      &:last-of-type {
        width: 60px;
      }
    }
  } 
`;

export default SocialMediaContainer;
