import BracketContainer from "./styles";

const BracketWrap = ({ children }) => {
  return (
    <BracketContainer className="BracketWrap">{children}</BracketContainer>
  );
};

export default BracketWrap;
